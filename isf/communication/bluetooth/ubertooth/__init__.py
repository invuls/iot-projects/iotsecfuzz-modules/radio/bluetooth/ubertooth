from isf.core import logger

try:
    from pyubertooth.ubertooth import Ubertooth
    from pyubertooth.bluetooth_packet import BtbbPacket
except SyntaxError:
    logger.error(
        'You installed pyubertooth for python2x. Download pyubertooth and use 2to3 script before installation')
    exit()


# TODO: add autoinstall pyubertooth with 2to3 lib
# TODO: also need to install libusb-32bit

class UbertoothISF:
    uber_obj = ''

    def __init__(self):
        try:
            self.uber_obj = Ubertooth()
        except:
            logger.error('Connect Ubertooth or reinstall drivers!')
            self.__del__()
            return

    def __del__(self):
        self.uber_obj.close()

    def sniffer(self, save_path="", channel=-1, timeout=10,
                package_count=100500):
        save_file = ''
        if save_path != '':
            logger.info('Saving data to: {}'.format(save_path))
            save_file = open(save_path, 'wb')

        if channel != -1:
            self.uber_obj.set_channel(channel)

        # TODO: check later both parameters together
        if package_count != 100500:
            for data in self.uber_obj.rx_stream(count=package_count):
                if save_file:
                    save_file.write(data)
                package = BtbbPacket(data=data)
                if package.LAP:
                    logger.debug(package)
        else:
            for data in self.uber_obj.rx_stream(secs=timeout):
                if save_file:
                    save_file.write(data)
                package = BtbbPacket(data=data)
                logger.info(str(data))
                if package.LAP:
                    logger.debug(package)

        if save_file:
            save_file.close()
